﻿using DaDataApp.Models;

namespace DaDataApp
{
    public static class Bootstrapper
    {
        public static IServiceCollection AddCore (this IServiceCollection services)
        {
            return services
                .AddSingleton<Result>();
        }
    }
}
