﻿using Dadata;
using Dadata.Model;
using Serilog;

namespace DaDataApp.Services
{
    public class DadataService
    {
        private readonly HttpClient _httpClient;
        public DadataService(HttpClient httpClient) 
        {
            _httpClient = httpClient;
        }
        public async Task<Address> GetAddress(string inputAddress)
        {
            var token = "cb14070e27acf1762a4206e6b42b7500b4bae434";
            var secret = "ae5315fc5258f61ef37c8468e810c7024ecfc737";
            var api = new CleanClientAsync(token, secret, "https://cleaner.dadata.ru/api/v1", _httpClient);
            try
            {
                var response = await api.Clean<Address>(inputAddress);
                Log.Information("Получен ответ с сервера");
                return response;
            }
            catch (Exception ex) 
            {
                Log.Error(ex.ToString());
                return null;
            }            
        }
    }
}
