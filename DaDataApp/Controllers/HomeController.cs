﻿using AutoMapper;
using Dadata;
using Dadata.Model;
using DaDataApp.Models;
using DaDataApp.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Diagnostics;
using System.Net.Http;

namespace DaDataApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMapper _mapper;
        private Result _result;
        private DadataService _daDataService;

        public HomeController(IMapper mapper, Result result, DadataService dadataService)
        {
            _mapper = mapper;
            _result = result;
            _daDataService = dadataService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index (string inputAddress)
        {
            try
            {
                Log.Information($"На вход дана строка:{inputAddress}");
                if (inputAddress == null)
                {
                    _result.Error = "Пустой ввод!";
                    Log.Information($"На вход дана пустая строка");
                    return View(_result);
                }
                Log.Information("Запрос на сервер DaData");
                var address = await _daDataService.GetAddress(inputAddress);        
                if (address == null)
                {
                    _result.Error = "Неизвестная ошибка!";
                    return View(_result);
                }
                if (address.result == null)
                {
                    _result.Error = "Некорректный адрес!";
                    Log.Information($"На вход дан некорректный адрес");
                }
                else
                {
                    _result = _mapper.Map<Result>(address);
                }

                return View(_result);
            }catch (Exception ex)
            {
                Log.Error(ex.ToString());
                _result.Error = "Неизвестная ошибка!";
                return View(_result);
            }
            
            //return Json(address);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}