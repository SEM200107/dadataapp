﻿using AutoMapper;
using Dadata.Model;

namespace DaDataApp.Models
{
    public class Result
    {
        public string FullAddress { get; set; } = string.Empty;
        public string Error { get; set; }
    }

    public class ResultProfile : Profile
    {
        public ResultProfile()
        {
            CreateMap<Address, Result>()
                .ForMember(dest => dest.FullAddress, opt => opt.MapFrom(src => src.result));
        }
    }
}
